<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Messenger</name>
   <tag></tag>
   <elementGuidId>3d080e25-1f24-454a-9057-737b36c6ced5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='u_0_0_h9']/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div._94vf._9pmv > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>แชทกับ สำนักคอมพิวเตอร์ มหาวิทยาลัยบูรพาสวัสดี มีอะไรให้เราช่วยไหมเข้าสู่ระบบ Messenger</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;u_0_0_h9&quot;)/div[1]/div[1]/div[@class=&quot;_94ve&quot;]/div[@class=&quot;_94vf _9pmv&quot;]/div[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_My ID/iframe_Change Password_f343d4a45f55f24</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='u_0_0_h9']/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'แชทกับ สำนักคอมพิวเตอร์ มหาวิทยาลัยบูรพาสวัสดี มีอะไรให้เราช่วยไหมเข้าสู่ระบบ Messenger' or . = 'แชทกับ สำนักคอมพิวเตอร์ มหาวิทยาลัยบูรพาสวัสดี มีอะไรให้เราช่วยไหมเข้าสู่ระบบ Messenger')]</value>
   </webElementXpaths>
</WebElementEntity>
