<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login success</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>8bd28bf3-9335-43d8-bd1a-30c4cdefca99</testSuiteGuid>
   <testCaseLink>
      <guid>e445900b-b480-42c8-9843-923826c49a45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f68ef918-4527-463c-8774-1fb479c1fd94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password 25</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e86b7a24-a7b0-4602-9a22-868c17d5a54e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b43f9fe8-7189-47fb-b599-a01ab42cdb68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password no Char</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c81957c1-1368-4250-a4be-874ed4246d81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password no Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28c68aac-bc8d-4e7f-a76d-890d22ba8e68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cff56d9-4d5f-4426-872c-8563ea83952f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password no String</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8bf87ef0-7b69-4bb4-9273-b3bb286f2b81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49181df1-32d5-4f8b-bd69-799db4e403e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bdb719b-e6ec-4244-9ba0-9e7f924e75bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
